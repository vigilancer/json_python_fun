#!/usr/bin/env python3

import json
from pathlib import Path
import argparse
import collections
import pprint
from datetime import datetime, timedelta
from itertools import chain


CountryDate = collections.namedtuple('CountryDate', ['country', 'date' ])
UserShort = collections.namedtuple('UserShort', ['name', 'email'])
DateUsers = collections.namedtuple('DateUsers', ['date', 'users'])
pp = pprint.PrettyPrinter(indent=2)


def restructure(users):
  rez = collections.defaultdict(lambda : collections.defaultdict(list))
  for man in users:
    for date_str in man['availableDates']:
      country = man['country']
      date = datetime.strptime(date_str, '%Y-%m-%d').date()
      u = UserShort("%s %s" % (man['firstName'], man['lastName']), man['email'])
      rez[country][date].append(u)
  return rez

def sortout(data, everyone_everyday):
  rez = collections.defaultdict(list)

  for country, dates in data.items():
    if len(dates) < 2:
        continue

    our_day = None
    our_users = {}
    # sorting to pick early date on collision
    for d, users in list(sorted(dates.items())):
      first_day = d
      second_day = d + timedelta(days=1)
      # that should filter out last day in a row and ranges with gaps
      if len(dates[second_day]) == 0:
        continue

      if everyone_everyday:
        two_days_users = list(set(dates[first_day]) & set(dates[second_day]))
      else:
        two_days_users = list(set(dates[first_day]) | set(dates[second_day]))

      if len(our_users) < len(two_days_users):
        our_day = first_day
        our_users = two_days_users

    rez[country] = DateUsers(our_day.isoformat(), list(our_users))
  return rez

def jsonout(data, file: Path):
  if file is None:
    print (json.dumps(data,indent=2))
  else:
    with open(str(file.resolve()), 'w') as out_file:
      json.dump(data, out_file, indent=2)


def main(file: Path):
  with open(str(file.resolve()), mode='r') as data_file:
    data = json.loads(data_file.read())

  rd = restructure(data)
  sd = sortout(rd, args.everyday)
  jsonout(sd, args.out)


def argparser():
  parser = argparse.ArgumentParser(description='testing json')
  parser.add_argument('-o', '--out', type=Path, required=False,
                      help='output json file. if ommited prints to stdout') 
  parser.add_argument('file', nargs=1, type=Path, 
                      help='valid json file for this particular test project')
  parser.add_argument('-e', '--everyday', action='store_true', default=False,
                      help='every user should be present on both days')
  return parser

if __name__ == '__main__':
  args = argparser().parse_args()
  main(args.file[0])

